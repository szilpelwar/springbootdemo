package com.sample.springboot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
    
    @RequestMapping("/user")
    public String getUser(){
        return "{firstName: Amishi, lastName: Shah}";
    }
}

